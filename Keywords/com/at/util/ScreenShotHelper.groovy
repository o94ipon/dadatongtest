package com.at.util

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import java.awt.image.BufferedImage

import javax.imageio.ImageIO

import org.omg.CORBA.Object
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import ru.yandex.qatools.ashot.AShot
import ru.yandex.qatools.ashot.Screenshot
import ru.yandex.qatools.ashot.comparison.ImageDiff
import ru.yandex.qatools.ashot.comparison.ImageDiffer



public class ScreenShotHelper {

	@Keyword
	public void TakeWebElementScreenShot(TestObject object) {
		WebElement element = WebUiCommonHelper.findWebElement(object, 0)
		WebDriver driver = DriverFactory.getWebDriver();

		Screenshot screenshot = new AShot().takeScreenshot(driver, element);
		ImageIO.write(screenshot.getImage(),"PNG",new File(System.getProperty("user.name")))
	}


	@Keyword
	public boolean compareImages(TestObject object){
		WebElement element = WebUiCommonHelper.findWebElement(object, 20)
		WebDriver driver = DriverFactory.getWebDriver();

		Screenshot getVerifycode = new AShot().takeScreenshot(driver, element);
		BufferedImage  beforeImage = getVerifycode.getImage();
		WebUI.click(findTestObject(element))
		WebUI.delay(1)
		BufferedImage afterImage = getVerifycode.getImage();

		ImageDiffer imgDiff = new ImageDiffer();
		ImageDiff diff = imgDiff.makeDiff(beforeImage, afterImage);

		return diff.hasDiff();
	}
}
