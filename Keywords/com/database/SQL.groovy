package com.database
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.SQLClientInfoException
import com.kms.katalon.core.annotation.Keyword
import java.sql.Connection;
import java.sql.Statement;

public class SQL {
	private static Connection connection = null;
	/**
	 * Open and return a connection to database
	 * @param dataFile absolute file path
	 * @return an instance of java.sql.Connection
	 */										
	@Keyword

	def connectDB(String url, String port, String dbname, String username, String password){

		//Load driver class for your specific database type
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver")

		String conn = "jdbc:sqlserver://" + url + ":" + port + ";" + "databaseName=" + dbname





		if(connection != null && !connection.isClosed()){

			connection.close()

		}

		connection = DriverManager.getConnection(conn,username,password)

		return connection

	}

	/**
	 * execute a SQL query on database
	 * @param queryString SQL query string
	 * @return a reference to returned data collection, an instance of java.sql.ResultSet
	 */



	@Keyword

	def executeQuery(String queryString) {

		Statement stm = connection.createStatement()

		ResultSet rs = stm.executeQuery(queryString)

		return rs

	}



	@Keyword

	def closeDatabaseConnection() {

		if(connection != null && !connection.isClosed()){

			connection.close()

		}

		connection = null

	}

	/**
	 * Execute non-query (usually INSERT/UPDATE/DELETE/COUNT/SUM...) on database
	 * @param queryString a SQL statement
	 * @return single value result of SQL statement
	 */

	@Keyword

	def execute(String queryString) {

		Statement stm = connection.createStatement()

		boolean result = stm.execute(queryString)

		return result

	}

	//透過URL取得ID 進而對資料庫撈取所需資料
	@Keyword
	//QIDdata(現在的URL,所需減去的URL長度,想要的欄位,前段SQL,後段SQL)
	def QIDdata (String nowUrl,int len,String columnN,String SQLApart , String SQLBpart){

		//減去長度後組成所需SQL
		String QID=nowUrl.substring(len)
		String QIDSQL =SQLApart+QID+SQLBpart

		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(QIDSQL);
		String dataFound = "";
		while (rs.next()) {
			dataFound = rs.getString(columnN);
		}
		return dataFound
		rs.close()
		stmt.close()
	}


}
