
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import com.kms.katalon.core.testobject.TestObject


def static "com.database.SQL.connectDB"(
    	String url	
     , 	String port	
     , 	String dbname	
     , 	String username	
     , 	String password	) {
    (new com.database.SQL()).connectDB(
        	url
         , 	port
         , 	dbname
         , 	username
         , 	password)
}

def static "com.database.SQL.executeQuery"(
    	String queryString	) {
    (new com.database.SQL()).executeQuery(
        	queryString)
}

def static "com.database.SQL.closeDatabaseConnection"() {
    (new com.database.SQL()).closeDatabaseConnection()
}

def static "com.database.SQL.execute"(
    	String queryString	) {
    (new com.database.SQL()).execute(
        	queryString)
}

def static "com.database.SQL.QIDdata"(
    	String nowUrl	
     , 	int len	
     , 	String columnN	
     , 	String SQLApart	
     , 	String SQLBpart	) {
    (new com.database.SQL()).QIDdata(
        	nowUrl
         , 	len
         , 	columnN
         , 	SQLApart
         , 	SQLBpart)
}

def static "com.at.util.ScreenShotHelper.TakeWebElementScreenShot"(
    	TestObject object	) {
    (new com.at.util.ScreenShotHelper()).TakeWebElementScreenShot(
        	object)
}

def static "com.at.util.ScreenShotHelper.compareImages"(
    	TestObject object	) {
    (new com.at.util.ScreenShotHelper()).compareImages(
        	object)
}

def static "com.datatool.getID.refreshBrowser"() {
    (new com.datatool.getID()).refreshBrowser()
}

def static "com.datatool.getID.clickElement"(
    	TestObject to	) {
    (new com.datatool.getID()).clickElement(
        	to)
}

def static "com.datatool.getID.getHtmlTableRows"(
    	TestObject table	
     , 	String outerTagName	) {
    (new com.datatool.getID()).getHtmlTableRows(
        	table
         , 	outerTagName)
}
