<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_scriptvar win  document</name>
   <tag></tag>
   <elementGuidId>981d13e9-5b32-4a2a-9ce1-179220c37778</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user-login-iframe</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>user-login-iframe</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>MMPaaSLoginPage?BLOCK=Iot&amp;USER_KEY=DE402B83456815CECB5A12B10FAF61D8&amp;BIP=10.5.6.180:8080&amp;AREA=I</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
					&lt;script>
						var win = document.getElementById(&quot;user-login-iframe&quot;).contentWindow;
							setTimeout(function(){
							win.postMessage(&quot;Hello Sparrow!&quot;,&quot;*&quot;);
							},1000);
					&lt;/script>
				</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;user-login-iframe&quot;)</value>
   </webElementProperties>
</WebElementEntity>
