<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_JoinSuccessed</name>
   <tag></tag>
   <elementGuidId>5eb2198d-8d74-45e3-9733-45afb452a392</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>html/body/p[count(. | //p[(text() = '註冊成功 請關閉本視窗後重新進行登入' or . = '註冊成功 請關閉本視窗後重新進行登入') and @ref_element = 'Object Repository/UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/iframe_JoinSuccessed']) = count(//p[(text() = '註冊成功 請關閉本視窗後重新進行登入' or . = '註冊成功 請關閉本視窗後重新進行登入') and @ref_element = 'Object Repository/UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/iframe_JoinSuccessed'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>註冊成功 請關閉本視窗後重新進行登入</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>html/body/p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/iframe_JoinSuccessed</value>
   </webElementProperties>
</WebElementEntity>
