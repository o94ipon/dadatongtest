<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign in</name>
   <tag></tag>
   <elementGuidId>6e5ba62d-7aa8-4fbc-a5a4-7f51fe66533c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;loginForm&quot;)/div[@class=&quot;login-box col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2&quot;]/div[@class=&quot;form-group no-margin-bottom&quot;]/button[@class=&quot;btn btn-primary btn-block&quot;][count(. | //button[@type = 'button' and @ref_element = 'Object Repository/UnLogin_Registered_OB/Page_LoginPart/iframe_MemberCenter']) = count(//button[@type = 'button' and @ref_element = 'Object Repository/UnLogin_Registered_OB/Page_LoginPart/iframe_MemberCenter'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-block</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>Login();</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loginForm&quot;)/div[@class=&quot;login-box col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2&quot;]/div[@class=&quot;form-group no-margin-bottom&quot;]/button[@class=&quot;btn btn-primary btn-block&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/UnLogin_Registered_OB/Page_LoginPart/iframe_MemberCenter</value>
   </webElementProperties>
</WebElementEntity>
