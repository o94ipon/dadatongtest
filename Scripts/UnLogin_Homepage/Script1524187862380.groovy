import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('http://10.5.6.180:8080/Iot')

WebUI.click(findTestObject('UnLogin_HomePage_OB/Page__/a_UserArea'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('UnLogin_HomePage_OB/Page__/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_HomePage_OB/Page__/h1_UserLogin'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_HomePage_OB/Page__/button_Sign in'), 1)

WebUI.click(findTestObject('UnLogin_HomePage_OB/Page__/span_X'))

WebUI.delay(1)

WebUI.focus(findTestObject('UnLogin_HomePage_OB/Page_UserCtrl/a_head-btn account'))

WebUI.click(findTestObject('UnLogin_HomePage_OB/Page_UserCtrl/a_RegistroArea'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('UnLogin_HomePage_OB/Page__/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_HomePage_OB/Page__/h1_JoinTheMemberShip'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_HomePage_OB/Page__/img_Commpany'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_HomePage_OB/Page__/img_ContactPhone'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_HomePage_OB/Page__/button_Join'), 1)

WebUI.click(findTestObject('UnLogin_HomePage_OB/Page__/span_X'))

WebUI.delay(1)

WebUI.focus(findTestObject('UnLogin_HomePage_OB/Page_UserCtrl/a_head-btn account'))

WebUI.click(findTestObject('UnLogin_HomePage_OB/Page_UserCtrl/a_LoginArea'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('UnLogin_HomePage_OB/Page__/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_HomePage_OB/Page__/h1_UserLogin'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_HomePage_OB/Page__/button_Sign in'), 1)

WebUI.click(findTestObject('UnLogin_HomePage_OB/Page__/span_X'))

WebUI.delay(3)

WebUI.closeBrowser()

