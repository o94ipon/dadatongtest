import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.awt.image.BufferedImage as BufferedImage
import org.omg.CORBA.Object as Object
import org.testng.Assert as Assert
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('http://10.5.6.180:8080/Iot')

WebUI.focus(findTestObject('UnLogin_Login_OB/Page_UserCtrl/a_head-btn account'))

WebUI.click(findTestObject('UnLogin_Login_OB/Page_UserCtrl/a_LoginArea'))

WebUI.delay(1)

WebUI.setText(findTestObject('UnLogin_Login_OB/Page__/input_dataMap.USER_ID'), 'English&123@com123.com')

WebUI.setText(findTestObject('UnLogin_Login_OB/Page__/input_dataMap.PASSWORD'), 'P@ssw0rd')

not_run: Assert.assertFalse(CustomKeywords.'com.at.util.ScreenShotHelper.compareImages'(findTestObject('Object Repository/UnLogin_Login_OB/Page__/img_Verification code')))

WebUI.click(findTestObject('UnLogin_Login_OB/Page__/button_Sign in'))

WebUI.delay(1)

WebUI.verifyTextPresent('ITU5TEST', false)

WebUI.closeBrowser()

