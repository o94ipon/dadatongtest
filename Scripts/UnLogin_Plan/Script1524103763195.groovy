import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://www.wpgdadago.com/Iot')

WebUI.delay(1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/a_Pop'))

WebUI.delay(1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/a_TheFirstPlan'))

WebUI.switchToWindowIndex(1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/div_Collection'))

WebUI.delay(1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/button_OK'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Unlogin_Plan_OB/Page_/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('Unlogin_Plan_OB/Page_/h1_UserLogin'), 1)

WebUI.verifyElementPresent(findTestObject('Unlogin_Plan_OB/Page_/button_Sign in'), 1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/span__X'))

WebUI.delay(1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/div_Score'))

WebUI.delay(1)

WebUI.delay(1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/button_OK'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Unlogin_Plan_OB/Page_/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('Unlogin_Plan_OB/Page_/h1_UserLogin'), 1)

WebUI.verifyElementPresent(findTestObject('Unlogin_Plan_OB/Page_/button_Sign in'), 1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/span__X'))

WebUI.delay(1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/div_Ask'))

WebUI.delay(1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/button_OK'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Unlogin_Plan_OB/Page_/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('Unlogin_Plan_OB/Page_/h1_UserLogin'), 1)

WebUI.verifyElementPresent(findTestObject('Unlogin_Plan_OB/Page_/button_Sign in'), 1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/span__X'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Unlogin_Plan_OB/Page_/h4__TechShare'), 1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/Img__Download'))

WebUI.delay(1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/button_OK'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Unlogin_Plan_OB/Page_/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('Unlogin_Plan_OB/Page_/h1_UserLogin'), 1)

WebUI.verifyElementPresent(findTestObject('Unlogin_Plan_OB/Page_/button_Sign in'), 1)

WebUI.click(findTestObject('Unlogin_Plan_OB/Page_/span__X'))

WebUI.delay(3)

WebUI.closeBrowser()

