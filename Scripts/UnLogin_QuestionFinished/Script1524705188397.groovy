import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement
import java.sql.Connection

//連接DB
CustomKeywords.'com.database.SQL.connectDB'('10.5.6.131', '1433', 'SPP_UAT', 'sqsadmin', 'wpgsqs16Q2')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://10.5.6.180:8080/Iot')

WebUI.delay(1)

WebUI.focus(findTestObject('UnLogin_QuestionFinished_OB/Page_List/a_QTypeList'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_List/a_QTypeBestAnser'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_List/a_Question'))

WebUI.delay(1)

WebUI.switchToWindowTitle('方案問答_問答內容_大大通')

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/a_QFollow'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_OK'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h1_UserLogin'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_Sign in'), 1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/span_X'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/a_QComment'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_OK'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h1_UserLogin'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_Sign in'), 1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/span_X'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/a_QReport'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_OK'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h1_UserLogin'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_Sign in'), 1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/span_X'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/a_AComment'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_OK'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h1_UserLogin'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_Sign in'), 1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/span_X'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/a_AReport'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_OK'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h1_UserLogin'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_Sign in'), 1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/span_X'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/Thumbs_Up'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_OK'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h1_UserLogin'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_Sign in'), 1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/span_X'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/Thumbs_Down'))

WebUI.delay(1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_OK'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h4_MemberCenter'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/h1_UserLogin'), 1)

WebUI.verifyElementPresent(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/button_Sign in'), 1)

WebUI.click(findTestObject('UnLogin_QuestionFinished_OB/Page_Question/span_X'))

WebUI.delay(3)

WebUI.closeBrowser()

