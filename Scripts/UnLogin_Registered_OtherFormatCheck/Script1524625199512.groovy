import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.stringtemplate.v4.compiler.CodeGenerator.region_return as region_return
import java.text.SimpleDateFormat as SimpleDateFormat

def date = new Date()

sdf = new SimpleDateFormat('yyyyMMddHHmmss')

'測試環境\r\n'
WebUI.openBrowser('http://10.5.6.180:8080/Iot')

WebUI.focus(findTestObject('UnLogin_Registered_OtherFormatCheck_OB/Page_UserCtrl/a_head-btn account'))

WebUI.click(findTestObject('UnLogin_Registered_OtherFormatCheck_OB/Page_UserCtrl/a_RegistroArea'))

WebUI.setText(findTestObject('UnLogin_Registered_OtherFormatCheck_OB/Page_RegisterPart/input_dataMap.EMAIL_ADDRESS'), sdf.format(
        date) + '@dubby.com')

WebUI.setText(findTestObject('UnLogin_Registered_OtherFormatCheck_OB/Page_RegisterPart/input_dataMap.CONTACT_NAME'), '嘟比')

WebUI.setText(findTestObject('UnLogin_Registered_OtherFormatCheck_OB/Page_RegisterPart/input_dataMap.PASSWORD'), 'P@ssw0rd')

WebUI.setText(findTestObject('UnLogin_Registered_OtherFormatCheck_OB/Page_RegisterPart/input_dataMap.CONFIRM_PASSWORD'), 
    'P@sswOrd')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.TAB))

WebUI.verifyElementVisible(findTestObject('UnLogin_Registered_OtherFormatCheck_OB/Page_RegisterPart/label_ConfirmPassword'))

WebUI.setText(findTestObject('UnLogin_Registered_OtherFormatCheck_OB/Page_RegisterPart/input_dataMap.securityCode'), '123')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.TAB))

WebUI.verifyElementVisible(findTestObject('UnLogin_Registered_OtherFormatCheck_OB/Page_RegisterPart/label_Non Verification'))

WebUI.closeBrowser()

