import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.text.SimpleDateFormat as SimpleDateFormat

def date = new Date()

sdf = new SimpleDateFormat('yyyyMMddHHmmss')

'測試環境\r\n'
WebUI.openBrowser('http://10.5.6.180:8080/Iot')

WebUI.focus(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_UserCtrl/a_head-btn account'))

WebUI.click(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_UserCtrl/a_RegistroArea'))

WebUI.delay(2)

WebUI.click(findTestObject('UnLogin_Registered_ReQuriedCheck_OB/Page_RegisterPart/button_Join'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/label_Required_email'))

WebUI.verifyElementVisible(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/label_Required_Name'))

WebUI.verifyElementVisible(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/label_Required_Password'))

WebUI.verifyElementVisible(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/label_Required_ConfirmPassword'))

WebUI.setText(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/input_dataMap.EMAIL_ADDRESS'), sdf.format(
        date) + '@5566NeverDie.com')

WebUI.verifyElementNotVisible(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/label_Required_email'))

WebUI.delay(1)

WebUI.setText(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/input_dataMap.CONTACT_NAME'), '嘟比Dog~->' + 
    sdf.format(date))

WebUI.verifyElementNotVisible(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/label_Required_Name'))

WebUI.delay(1)

WebUI.setText(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/input_dataMap.PASSWORD'), 'P@ssw0rd')

WebUI.verifyElementNotVisible(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/label_Required_Password'))

WebUI.delay(1)

WebUI.setText(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/input_dataMap.CONFIRM_PASSWORD'), 'P@ssw0rd')

WebUI.verifyElementNotVisible(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/label_Required_ConfirmPassword'))

'缺少步驟輸入驗證碼\r\n'
WebUI.click(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/button_Join'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('UnLogin_Registered_RequriedCheck_OB/Page_RegisterPart/p_JoinSuccessed'), 0)

WebUI.delay(1)

WebUI.closeBrowser()

